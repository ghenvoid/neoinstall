#!/bin/sh

my_base='bash-completion
pacman-contrib
iwd
networkmanager
neovim
git
xdg-utils
xdg-user-dirs
pipewire
pipewire-alsa
pipewire-jack
pipewire-pulse
gst-plugin-pipewire
pipewire-v4l2
wireplumber
btrfs-progs
exfatprogs
dosfstools
noto-fonts
noto-fonts-emoji
aria2
intel-ucode
amd-ucode
zram-generator
tmux
power-profiles-daemon'

my_drivers='mesa
lib32-mesa
libva-mesa-driver
lib32-libva-mesa-driver
mesa-vdpau
lib32-mesa-vdpau
libva-intel-driver
lib32-libva-intel-driver
intel-media-driver
libva-vdpau-driver
lib32-libva-vdpau-driver
libvdpau-va-gl
cups'

my_gui='xorg
xorg-xinit
alacritty
flatpak
flatpak-xdg-utils
pacmanlogviewer
helvum
easyeffects
firefox
mpv'

my_qt='xdg-desktop-portal-kde
sddm
discover
packagekit-qt5
breeze
breeze-gtk
pavucontrol-qt
pcmanfm-qt
gvfs
gvfs-mtp
network-manager-applet'

my_kde='xdg-desktop-portal-kde
plasma-meta
kde-accessibility-meta
kde-education-meta
kde-games-meta
kde-graphics-meta
kde-multimedia-meta
kde-network-meta
kde-pim-meta
kde-sdk-meta
kde-system-meta
kde-utilities-meta
packagekit-qt5
plasma-wayland-session
firewalld
libappimage
icoutils
kimageformats
qt5-imageformats
ffmpegthumbs
kdegraphics-thumbnailers
system-config-printer
cups-pk-helper
xsettingsd
qbittorrent'

my_gtk='xdg-desktop-portal-gtk
lightdm
lightdm-gtk-greeter
gnome-software
gnome-software-packagekit-plugin
pavucontrol
pcmanfm
gvfs
gvfs-mtp
network-manager-applet'

my_gnome='xdg-desktop-portal-gnome
gnome
gnome-software-packagekit-plugin
transmission-gtk'

while :
do
	printf "\nGenerate and overwrite fstab of / in /etc/fstab? [yes/no]: "
	read -r my_fstab
	case "$my_fstab" in
		[Yy][Ee][Ss])
			genfstab -U / > /etc/fstab
			break
			;;
		[Nn][Oo])
			break
			;;
		*)
			;;
	esac
done

##VARIABLES DECLARATION
bios_uefi="$(ls /sys/firmware/efi/efivars > /dev/null 2>&1 && echo "uefi" || echo "bios")"
block_device="$(lsblk -ndpo PKNAME "$(findmnt -nvo SOURCE -T /)")"
while :
do
	printf '\nPC running in %s mode: ' "$bios_uefi"
	case "$bios_uefi" in
		bios)
			printf '\nGRUB Bootloader will be installed at %s' "$block_device"
			sleep 3
			break
			;;
		uefi)
			printf '\nGRUB Bootloader and rEFInd will be installed at %s and /boot/efi' "$block_device"
			sleep 3
			break
			;;
		[Nn][Oo][Nn][Ee])
			bios_uefi='none'
			break
			;;
		*)
			;;
	esac
done

printf "\nDefine the hostname: "
read -r my_hostname
printf "\nDefine your username: "
read -r my_username
printf "\nDefine the root's password: "
read -sr my_rootp
printf "\nDefine you user's password: "
read -sr my_userp

while :
do
	printf "\nEnable AUR (PARU)? [yes/no]: "
	read -r my_aur
	case "$my_aur" in
		[Yy][Ee][Ss])
			my_aur='yes'
			break
			;;
		[Nn][Oo])
			my_aur='no'
			break
			;;
		*)
			;;
	esac
done

while :
do
	printf "\nEnable Snap? (Requires AUR) [yes/no]: "
	read -r my_snap
	case "$my_snap" in
		[Yy][Ee][Ss])
			my_snap='yes'
			break
			;;
		[Nn][Oo])
			my_snap='no'
			break
			;;
		*)
			;;
	esac
done

while :
do
	printf "\nDesktop Environment or Base install?: (base; kde; gnome; xfce; lxqt): "
	read -r de_wm
	case "$de_wm" in
		[Bb][Aa][Ss][Ee])
			de_wm='base'
			break
			;;
		[Kk][Dd][Ee])
			de_wm='kde'
			break
			;;
		[Gg][Nn][Oo][Mm][Ee])
			de_wm='gnome'
			break
			;;
		[Xx][Ff][Cc][Ee])
			de_wm='xfce'
			break
			;;
		[Ll][Xx][Qq][Tt])
			de_wm='lxqt'
			break
			;;
		*)
			;;
	esac
done

while :
do
	printf "\nPortugues ou/or English? (p/e): "
	read -r my_locale
	case "$my_locale" in
		[Pp])
			sed -i '/#pt_BR.UTF-8 UTF-8/c\pt_BR.UTF-8 UTF-8' /etc/locale.gen
			#sed -i '/#pt_BR ISO-8859-1/c\pt_BR ISO-8859-1' /etc/locale.gen
			locale-gen
			echo 'LANG=pt_BR.UTF-8' > /etc/locale.conf
			echo 'KEYMAP=br-abnt2' > /etc/vconsole.conf
			break
			;;
		[Ee])
			sed -i '/#en_US.UTF-8 UTF-8/c\en_US.UTF-8 UTF-8' /etc/locale.gen
			#sed -i '/#en_US ISO-8859-1/c\en_US ISO-8859-1' /etc/locale.gen
			locale-gen
			echo 'LANG=en_US.UTF-8' > /etc/locale.conf
			break
			;;
		*)
			;;
	esac
done

##BASE CONFIG
ln -sf /usr/share/zoneinfo/America/Recife /etc/localtime
hwclock --systohc
echo "$my_hostname" > /etc/hostname
cat << HOSTS > /etc/hosts
127.0.0.1	localhost
::1		localhost
127.0.1.1	"$my_hostname".localdomain	"$my_hostname"
HOSTS

##SETTING PACMAN
sed -i '/#Color/c\Color' /etc/pacman.conf
sed -i '/ParallelDownloads/c\ParallelDownloads = 10' /etc/pacman.conf
sed -i '/#\[multilib]/c\[multilib]' /etc/pacman.conf
sed -i '/\[multilib]/a\Include = /etc/pacman.d/mirrorlist' /etc/pacman.conf

until pacman -Syu --noconfirm --needed sudo reflector
do
	echo 'Pacman failed, trying again...'
done

reflector --verbose --threads "$(nproc --all)" --protocol https --latest 20 --sort rate --save /etc/pacman.d/mirrorlist
echo ""

##ADDITIONAL CONFIG
useradd -mG wheel "$my_username"
echo root:"$my_rootp" | chpasswd && echo "Root's password set!"
echo "$my_username":"$my_userp" | chpasswd && echo "User's password set!"
sudo sed -i '/# %wheel ALL=(ALL:ALL) ALL/c\ %wheel ALL=(ALL:ALL) ALL' /etc/sudoers
timedatectl set-ntp true
sleep 3

until pacman -Syu --noconfirm --needed $(echo "$my_base")
do
	echo "Pacman failed, trying again..."
	sleep 3
done

while :
do
	case "$bios_uefi" in
		bios)
			until pacman -Syu --noconfirm --needed grub grub-btrfs os-prober
			do
				echo 'Pacman failed, trying again...'
				sleep 3
			done
			grub-install --target=i386-pc "$block_device"
			sed -i '/GRUB_CMDLINE_LINUX_DEFAULT="loglevel=3 quiet"/c\GRUB_CMDLINE_LINUX_DEFAULT="loglevel=3 mitigations=off sysrq_always_enabled=1 nowatchdog"\nGRUB_DISABLE_OS_PROBER=false' /etc/default/grub
			grub-mkconfig -o /boot/grub/grub.cfg
			break
			;;
		uefi)
			until pacman -Syu --noconfirm --needed refind grub grub-btrfs os-prober efibootmgr
			do
				echo 'Pacman failed, trying again...'
				sleep 3
			done
			mkdir -p /boot/efi/EFI/BOOT
			grub-install --target=x86_64-efi --efi-directory=/boot/efi --bootloader-id=GRUB --removable
			sed -i '/GRUB_CMDLINE_LINUX_DEFAULT="loglevel=3 quiet"/c\GRUB_CMDLINE_LINUX_DEFAULT="mitigations=off sysrq_always_enabled=1 nowatchdog loglevel=3"\nGRUB_DISABLE_OS_PROBER=false' /etc/default/grub
			sync
			grub-mkconfig -o /boot/grub/grub.cfg
			#mkdir -p /boot/efi/EFI/refind/drivers_x64
			#cp -r /usr/share/refind /boot/efi/EFI/
			#cp /usr/share/refind/refind_x64.efi /boot/efi/EFI/BOOT/bootx64.efi
			#cp /usr/share/refind/refind.conf-sample /boot/efi/EFI/refind/refind.conf
			#part_uuid="$(findmnt -n -o PARTUUID -T /)"
			#subvol_root="$(findmnt -n -o FSROOT -T / | sed 's/\///')"
			#sed -i '/#extra_kernel_version_strings/c\extra_kernel_version_strings linux-hardened,linux-zen,linux-lts,linux-ck,linux-pf,linux' /boot/efi/EFI/refind/refind.conf
			#if [ "$(findmnt -n -o FSTYPE -T /)" = btrfs ]; then
			#cat << BTRFS > /boot/refind_linux.conf
			#"Boot using standard options"  "root=PARTUUID=$part_uuid rw add_efi_memmap initrd=$subvol_root\boot\intel-ucode.img initrd=$subvol_root\boot\amd-ucode.img rootflags=subvol=$subvol_root initrd=$subvol_root\boot\initramfs-%v.img"
			#"Boot using standard options"  "root=PARTUUID=$part_uuid rw add_efi_memmap initrd=$subvol_root\boot\intel-ucode.img initrd=$subvol_root\boot\amd-ucode.img rootflags=subvol=$subvol_root initrd=$subvol_root\boot\initramfs-%v-fallback.img"
			#"Boot using standard options"  "root=PARTUUID=$part_uuid rw add_efi_memmap initrd=$subvol_root\boot\intel-ucode.img initrd=$subvol_root\boot\amd-ucode.img rootflags=subvol=$subvol_root initrd=$subvol_root\boot\initramfs-%v.img systemd.unit=multi-user.target"
#BTRFS
			#else
			#cat << REFIND > /boot/refind_linux.conf
			#"Boot using standard options"  "root=PARTUUID=$part_uuid rw add_efi_memmap initrd=boot\intel-ucode.img initrd=boot\amd-ucode.img initrd=\boot\initramfs-%v.img"
			#"Boot using standard options"  "root=PARTUUID=$part_uuid rw add_efi_memmap initrd=boot\intel-ucode.img initrd=boot\amd-ucode.img initrd=\boot\initramfs-%v-fallback.img"
			#"Boot using standard options"  "root=PARTUUID=$part_uuid rw add_efi_memmap initrd=boot\intel-ucode.img initrd=boot\amd-ucode.img initrd=\boot\initramfs-%v.img systemd.unit=multi-user.target"			
#REFIND
			#fi
			break
			;;
		none)
			break
			;;
		*)
			;;
	esac
done

while :
do
	case "$de_wm" in
		[Bb][Aa][Ss][Ee])
			break
			;;
		[Kk][Dd][Ee])
			until pacman -Syu --noconfirm --needed $(echo "$my_drivers $my_gui $my_kde")
			do
				echo "Pacman failed, trying again..."
				sleep 3
			done
			systemctl enable NetworkManager
			systemctl enable cups.socket
			systemctl enable sddm
			flatpak install flathub org.kde.ark org.cubocore.CoreKeyboard
			break
			;;
		[Gg][Nn][Oo][Mm][Ee])
			until pacman -Syu --noconfirm --needed $(echo "$my_drivers $my_gui $my_gnome")
			do
				echo "Pacman failed, tryin again..."
				sleep 3
			done
			systemctl enable NetworkManager
			systemctl enable gdm
			flatpak install flathub org.gnome.FileRoller org.cubocore.CoreKeyboard
			break
			;;
		[Xx][Ff][Cc][Ee])
			until pacman -Syu --noconfirm --needed $(echo "$my_drivers $my_gui $my_gtk") xfce4 xfce4-goodies tumbler thunar-volman thunar-archive-plugin thunar-media-tags-plugin onboard
			do
				echo "Pacman failed, trying again..."
				sleep 3
			done
			systemctl enable NetworkManager
			systemctl enable lightdm
			flatpak install flathub org.gnome.FileRoller org.cubocore.CoreKeyboard
			break
			;;
		[Ll][Xx][Qq][Tt])
			until pacman -Syu --noconfirm --needed $(echo "$my_drivers $my_gui $my_qt") lxqt kwin papirus-icon-theme
			do
				echo "Pacman failed, trying again..."
				sleep 3
			done
			systemctl enable NetworkManager
			systemctl enable sddm
			flatpak install flathub org.kde.ark org.cubocore.CoreKeyboard
			break
			;;
		*)
			;;
	esac
done

systemctl enable systemd-oomd.service
systemctl enable reflector.timer
mkdir -p /etc/systemd

cat << ZRAM > /etc/systemd/zram-generator.conf
# This file is part of the zram-generator project
# https://github.com/systemd/zram-generator

[zram0]
host-memory-limit = none
zram-fraction = 1
compression-algorithm = zstd
ZRAM
mkdir -p /etc/sysctl.d/
cat << SWAPPINESS > /etc/sysctl.d/99-swappiness.conf
vm.swappiness=200
SWAPPINESS

while :
do
	case "$my_aur" in
		[Yy][Ee][Ss])
			until pacman -Syu --noconfirm --needed base-devel
			do
				echo "Pacman failed, trying again..."
				sleep 3
			done
			su - "$my_username" <<AUR
			cd /home/"$my_username"/
			git clone https://aur.archlinux.org/paru-bin.git
			cd /home/"$my_username"/paru-bin/
			makepkg --syncdeps --cleanbuild --clean --needed --noconfirm
			cd /home/"$my_username"/
AUR
			pacman -U --needed --noconfirm /home/"$my_username"/paru-bin/"$(find /home/"$my_username"/paru-bin/ -name "*.pkg.tar.zst" | sed 's|/.*/||g')"
			break
			;;
		[Nn][Oo])
			break
			;;
		*)
			;;
	esac
done

while :
do
	case "$my_snap" in
		[Yy][Ee][Ss])
			until pacman -Syu --noconfirm --needed base-devel
			do
				echo "Pacman failed, trying again..."
				sleep 3
			done
			su - "$my_username" <<SNAP
			cd /home/"$my_username"/
			git clone https://aur.archlinux.org/snapd.git
			cd /home/"$my_username"/snapd/
			makepkg --syncdeps --cleanbuild --clean --needed --noconfirm
			cd /home/"$my_username"/
SNAP
			pacman -U --needed --noconfirm /home/"$my_username"/snapd/"$(find /home/"$my_username"/snapd/ -name "*.pkg.tar.zst" | sed 's|/.*/||g')"
			break
			;;
		[Nn][Oo])
			break
			;;
		*)
			;;
	esac
done

echo ''
echo 'Almost done...'
sync
echo 'Done. Exit, swapoff and umount -a.'
