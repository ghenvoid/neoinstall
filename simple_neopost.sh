#!/bin/sh

##VARIABLES DECLARATION
echo ""
printf "\nType the hostname: "
read -r my_hostname

bios_uefi="$(ls /sys/firmware/efi/efivarss > /dev/null 2>&1 && echo "uefi" || echo "bios")"
echo ""
while :
do
	printf "\nDo you want a bootloader? yes/no: "
	read -r my_bootloader
	echo ""
	case "$bios_uefi" in
		bios)
            printf "\nGRUB Bootloader will be installed at $(findmnt -nvo SOURCE -T $(pwd))\n"
			sleep 5
			break
			;;
		uefi)
			printf "\nGRUB Bootloader/rEFInd will be installed at $(findmnt -nvo SOURCE -T $(pwd)) and /boot/efi"
			break
			;;
		[Nn][Oo][Nn][Ee])
			bios_uefi='none'
			break
			;;
		*)
			;;
	esac
done

##BASE CONFIG

ln -sf /usr/share/zoneinfo/America/Recife /etc/localtime
hwclock --systohc
sed -i '/#pt_BR.UTF-8 UTF-8/c\pt_BR.UTF-8 UTF-8' /etc/locale.gen
sed -i '/#en_US.UTF-8 UTF-8/c\en_US.UTF-8 UTF-8' /etc/locale.gen
locale-gen
echo 'LANG=en_US.UTF-8' > /etc/locale.conf
echo "$my_hostname" > /etc/hostname
cat << HOSTS > /etc/hosts
127.0.0.1	localhost
::1			localhost
127.0.1.1	"$my_hostname".localdomain	"$my_hostname"
HOSTS

##EXTRAS

#until pacman -Syu --noconfirm --needed base base-devel bash-completion pacman-contrib iwd networkmanager neovim git xdg-utils xdg-user-dirs pipewire pipewire-pulse pipewire-alsa pipewire-media-session mpd mpc btrfs-progs exfatprogs ntfs-3g dosfstools noto-fonts noto-fonts-emoji aria2 intel-ucode amd-ucode zram-generator tmux
#do
#	echo "Pacman failed, trying again..."
#	sleep 3
#done

while :
do
	case "$bios_uefi" in
		bios)
			until pacman -Syu --noconfirm --needed grub grub-btrfs os-prober
			do
				echo 'Pacman failed, trying again...'
				sleep 3
			done
			grub-install --target=i386-pc "$block_device"
			sed -i '/GRUB_CMDLINE_LINUX_DEFAULT="loglevel=3 quiet"/c\GRUB_CMDLINE_LINUX_DEFAULT="loglevel=3 mitigations=off sysrq_always_enabled=1 nowatchdog"\nGRUB_DISABLE_OS_PROBER=false' /etc/default/grub
			grub-mkconfig -o /boot/grub/grub.cfg
			break
			;;
		uefi)
			until pacman -Syu --noconfirm --needed refind grub grub-btrfs os-prober efibootmgr
			do
				echo 'Pacman failed, trying again...'
				sleep 3
			done
			grub-install --target=x86_64-efi --efi-directory=/boot/efi --bootloader-id=GRUB --removable
			sed -i '/GRUB_CMDLINE_LINUX_DEFAULT="loglevel=3 quiet"/c\GRUB_CMDLINE_LINUX_DEFAULT="mitigations=off sysrq_always_enabled=1 nowatchdog loglevel=3"\nGRUB_DISABLE_OS_PROBER=false' /etc/default/grub
			grub-mkconfig -o /boot/grub/grub.cfg
			mkdir -p /boot/efi/EFI/BOOT
			mkdir -p /boot/efi/EFI/refind/drivers_x64
			cp -r /usr/share/refind /boot/efi/EFI/
			cp /usr/share/refind/refind_x64.efi /boot/efi/EFI/BOOT/bootx64.efi
			cp /usr/share/refind/refind.conf-sample /boot/efi/EFI/refind/refind.conf
			part_uuid="$(findmnt -n -o PARTUUID -T /)"
			subvol_root="$(findmnt -n -o FSROOT -T / | sed 's/\///')"
			sed -i '/#extra_kernel_version_strings/c\extra_kernel_version_strings linux-hardened,linux-zen,linux-lts,linux-ck,linux-pf,linux' /boot/efi/EFI/refind/refind.conf
			if [ "$(findmnt -n -o FSTYPE -T /)" = btrfs ]; then
			cat << BTRFS > /boot/refind_linux.conf
			"Boot using standard options"  "root=PARTUUID=$part_uuid rw add_efi_memmap initrd=$subvol_root\boot\intel-ucode.img initrd=$subvol_root\boot\amd-ucode.img rootflags=subvol=$subvol_root initrd=$subvol_root\boot\initramfs-%v.img"
			"Boot using standard options"  "root=PARTUUID=$part_uuid rw add_efi_memmap initrd=$subvol_root\boot\intel-ucode.img initrd=$subvol_root\boot\amd-ucode.img rootflags=subvol=$subvol_root initrd=$subvol_root\boot\initramfs-%v-fallback.img"
			"Boot using standard options"  "root=PARTUUID=$part_uuid rw add_efi_memmap initrd=$subvol_root\boot\intel-ucode.img initrd=$subvol_root\boot\amd-ucode.img rootflags=subvol=$subvol_root initrd=$subvol_root\boot\initramfs-%v.img systemd.unit=multi-user.target"
BTRFS
			else
			cat << REFIND > /boot/refind_linux.conf
			"Boot using standard options"  "root=PARTUUID=$part_uuid rw add_efi_memmap initrd=boot\intel-ucode.img initrd=boot\amd-ucode.img initrd=\boot\initramfs-%v.img"
			"Boot using standard options"  "root=PARTUUID=$part_uuid rw add_efi_memmap initrd=boot\intel-ucode.img initrd=boot\amd-ucode.img initrd=\boot\initramfs-%v-fallback.img"
			"Boot using standard options"  "root=PARTUUID=$part_uuid rw add_efi_memmap initrd=boot\intel-ucode.img initrd=boot\amd-ucode.img initrd=\boot\initramfs-%v.img systemd.unit=multi-user.target"			
REFIND
			fi
			break
			;;
		none)
			break
			;;
		*)
			;;
	esac
done
